import Vue from "https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.esm.browser.js";
import peopleCmp from "./people-cmp.js";
import dateCmp from "./date-cmp.js";

const div = document.createElement("div");
document.body.appendChild(div);

const appCmp = Vue.component('app-cmp', {
    components: { peopleCmp, dateCmp },
    data() {
        return {
            dt: new Date(),
        };
    },
    template: `<div>
        <date-cmp 
            v-model="dt"
        />
        <people-cmp
            :dt="dt"
        />
    </div>`,
});

new Vue({el: div, render: h => h(appCmp)});

