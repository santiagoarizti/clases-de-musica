import Vue from "https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.esm.browser.js";
import personCmp from "./person-cmp.js";

const peopleCmp = Vue.component('people-cmp', {
    components: {personCmp},
    props: [
        "dt" // instance of Date
    ],
    data() {
        return {
            people: [],
        };
    },
    methods: {
        addPerson() {
            // name, price, weekDay
            this.people.push({data: ["", 0, 0]});
        },
        deletePerson(p) {
            this.people.splice(this.people.indexOf(p));
        },
    },
    computed: {
        daysCnt() {
            const month = this.dt.getMonth();
            const year = this.dt.getFullYear();
            const daysCnt = [0, 0, 0, 0, 0, 0, 0];
            for (let i = 0; i < 31; i++) {
                const dt = new Date(year, month, i+1);
                if (dt.getMonth() !== month) continue;
                daysCnt[dt.getDay()]++;
            }
            return daysCnt;
        },
        total() {
            return this.people.reduce((c, p) => {
                const [name, price, weekDay] = p.data;
                return c + price * this.daysCnt[weekDay];
            }, 0);
        },
    },
    template: `<div>
        <div>
            <button @click="addPerson">+</button>
        </div>
        <person-cmp v-for="(p, i) in people"
            :key="i"
            v-model="p.data"
            @delete="deletePerson(p)"
        />
        <div>
            Total: {{total}}
        </div>
    </div>`,
});

export default peopleCmp;
