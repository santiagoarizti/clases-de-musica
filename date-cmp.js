import Vue from "https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.esm.browser.js";

const dateCmp = Vue.component('date-cmp', {
    props: [
        "value" // instance of Date
    ],
    data() {
        return {
            months: [
                {val: "0", label: "Enero"},
                {val: "1", label: "Febrero"},
                {val: "2", label: "Marzo"},
                {val: "3", label: "Abril"},
                {val: "4", label: "Mayo"},
                {val: "5", label: "Junio"},
                {val: "6", label: "Julio"},
                {val: "7", label: "Agosto"},
                {val: "8", label: "Septiembre"},
                {val: "9", label: "Octubre"},
                {val: "10", label: "Noviembre"},
                {val: "11", label: "Diciembre"},
            ],
            month: "" + this.value.getMonth(),
            year: "" + this.value.getFullYear(),
        };
    },
    computed: {
        output() {
            return new Date(parseInt(this.year), parseInt(this.month));
        },
    },
    watch: {
        output() {
            this.$emit("input", this.output);
        }
    },
    template: `<div style="display: flex">
        <span>
            <input type="number" v-model="year">
        </span>
        <span>
            <select v-model="month">
                <option v-for="m in months" :value="m.val">{{m.label}}</option>
            </select>
        </span>
    </div>`,
});

export default dateCmp;
