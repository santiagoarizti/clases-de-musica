import Vue from "https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.esm.browser.js";

const personCmp = Vue.component('person-cmp', {
    props: [
        "value"
    ],
    data() {
        return {
            name: this.value[0],
            price: this.value[1],
            weekDay: "" + this.value[2],
        };
    },
    computed: {
        weekDays() {
            return [
                {val: "0", label: "Domingo"},
                {val: "1", label: "Lunes"},
                {val: "2", label: "Martes"},
                {val: "3", label: "Miércoles"},
                {val: "4", label: "Jueves"},
                {val: "5", label: "Viernes"},
                {val: "6", label: "Sábado"},
            ];
        },
        output() {
            return [this.name, parseInt(this.price), parseInt(this.weekDay)];
        }
    },
    watch: {
        output() { this.$emit("input", this.output); },
    },
    template: `<div style="display: flex;">
        <span><input type="text" v-model="name" placeholder="name"></span>
        <span><input type="number" v-model="price" placeholder="price" min="0" max="200" step="25"></span>
        <span>
            <select v-model="weekDay">
                <option v-for="d in weekDays" :value="d.val">{{d.label}}</option>
            </select>
        </span>
        <span><button @click="$emit('delete')">-</button></span>
    </div>`,
});

export default personCmp;
